package cn.kt.springbootuploadmaster.service;

import cn.kt.springbootuploadmaster.domin.FileChunkParam;
import cn.kt.springbootuploadmaster.domin.LocalStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by tao.
 * Date: 2022/7/1 10:41
 * 描述:
 */
public interface LocalStorageService {
    /**
     * 根据文件 md5 查询
     *
     * @param md5 md5
     * @return
     */
    LocalStorage findByMd5(String md5);

    /**
     * 保存记录
     *
     * @param localStorage 记录参数
     */
    void saveLocalStorage(LocalStorage localStorage);

    /**
     * 保存记录
     *
     * @param param 记录参数
     */
    void saveLocalStorage(FileChunkParam param);

    /**
     * 删除记录
     *
     * @param localStorage localStorage
     * @return
     */
    void delete(LocalStorage localStorage);

    /**
     * 根据 id 删除
     *
     * @param id id
     * @return
     */
    void deleteById(Long id);

    void downloadByName(String name, String md5, HttpServletRequest request, HttpServletResponse response);
}
