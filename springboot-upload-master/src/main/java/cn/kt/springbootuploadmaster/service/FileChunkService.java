package cn.kt.springbootuploadmaster.service;

import cn.kt.springbootuploadmaster.domin.FileChunkParam;

import java.util.List;

/**
 * Created by tao.
 * Date: 2022/6/29 10:53
 * 描述:
 */
public interface FileChunkService {
    /**
     * 根据文件 md5 查询
     *
     * @param md5 md5
     * @return
     */
    List<FileChunkParam> findByMd5(String md5);

    /**
     * 保存记录
     *
     * @param param 记录参数
     */
    void saveFileChunk(FileChunkParam param);

    /**
     * 删除记录
     *
     * @param fileChunk fileChunk
     * @return
     */
    void delete(FileChunkParam fileChunk);

    /**
     * 根据 id 删除
     *
     * @param id id
     * @return
     */
    void deleteById(Long id);
}
