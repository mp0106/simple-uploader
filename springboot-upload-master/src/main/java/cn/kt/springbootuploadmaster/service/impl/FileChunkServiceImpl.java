package cn.kt.springbootuploadmaster.service.impl;

import cn.kt.springbootuploadmaster.domin.FileChunkParam;
import cn.kt.springbootuploadmaster.repository.FileChunkRepository;
import cn.kt.springbootuploadmaster.service.FileChunkService;
import cn.kt.springbootuploadmaster.service.LocalStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * Created by tao.
 * Date: 2022/6/29 10:53
 * 描述:
 */
@Service
public class FileChunkServiceImpl implements FileChunkService {
    @Autowired
    private FileChunkRepository fileChunkRepository;

    @Autowired
    private LocalStorageService localStorageService;

    @Override
    public List<FileChunkParam> findByMd5(String md5) {
        return fileChunkRepository.findByIdentifier(md5);
    }

    @Override
    public void saveFileChunk(FileChunkParam param) {
        fileChunkRepository.save(param);
        // 当文件分片完整上传完成，存一份在LocalStorage表中
        if (param.getChunkNumber().equals(param.getTotalChunks())) {
            localStorageService.saveLocalStorage(param);
        }
    }

    @Override
    public void delete(FileChunkParam fileChunk) {
        fileChunkRepository.delete(fileChunk);
    }

    @Override
    public void deleteById(Long id) {
        fileChunkRepository.deleteById(id);
    }
}
