package cn.kt.springbootuploadmaster.repository;

import cn.kt.springbootuploadmaster.domin.LocalStorage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by tao.
 * Date: 2022/7/1 9:29
 * 描述:
 */
public interface LocalStorageRepository extends JpaRepository<LocalStorage, Long>, JpaSpecificationExecutor<LocalStorage> {
    LocalStorage findByIdentifier(String identifier);

    LocalStorage findByRealNameAndIdentifier(String name, String md5);
}
